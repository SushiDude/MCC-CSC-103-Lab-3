/* Written by Fig and Wyatt J. Brown.
 * Due date: 2015-11-04
 * Description: A program that implements and tests a class that represents an
 * abstract linked list of doubles. */
package edu.monroecc.student.jruiz13ANDwbrown49.lab3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/** The class {@code Lab3} reads input data from a file in order to run a
 * {@code SequenceTest}.
 *
 * @author Fig
 * @author Wyatt J. Brown
 * @see SequenceTest */
public class Lab3
{
	/** Reads data from the input file and passes it to a SequenceTest object.
	 *
	 * @param args an array of Strings representing the command line arguments.
	 * @throws IOException if the input file can not be read.
	 * @see SequenceTest#menu(String) */
	public static void main(final String[] args) throws IOException
	{
		final SequenceTest sequenceTest = new SequenceTest();
		// Open the file and begin processing as input.
		final BufferedReader bufferedReader =
				new BufferedReader(new FileReader("input.txt"));
		for (String line = bufferedReader.readLine(); line != null; line =
				bufferedReader.readLine())// Read file line by line.
			sequenceTest.menu(line);

		bufferedReader.close();// Close the file.
		System.exit(0);// Exit cleanly.
	}
}
