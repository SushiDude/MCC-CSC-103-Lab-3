package edu.monroecc.student.jruiz13ANDwbrown49.lab3;

/* This Node class needs to store a double for the DoubleLinkedSeq.
 * This differs from the provided Node class that stores an int. */

public class Node
{
	private double data;
	private Node link;

	public Node()
	{
		data = 0;
		link = null;
	}

	public Node(final double d, final Node l)
	{
		data = d;
		link = l;
	}

	double getData()
	{
		return data;
	}

	Node getLink()
	{
		return link;
	}

	void setData(final double newElem)
	{
		data = newElem;
	}

	void setLink(final Node newNode)
	{
		link = newNode;
	}
}
