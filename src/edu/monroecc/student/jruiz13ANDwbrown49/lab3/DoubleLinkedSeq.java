/* Written by Fig and Wyatt J. Brown.
 * Due date: 2015-11-04
 * Description: A program that implements and tests a class that represents an
 * abstract linked list of doubles. */
// File: DoubleLinkedSeq.java from the package edu.colorado.collections

// This is an assignment for students to complete after reading Chapter 4 of
// "Data Structures and Other Objects Using Java" by Michael Main.

// Check with your instructor to see whether you should put this class in
// a package. At the moment, it is declared as part of edu.colorado.collections:
//package edu.colorado.collections;
package edu.monroecc.student.jruiz13ANDwbrown49.lab3;

/** This class is a homework assignment; A DoubleLinkedSeq is a collection of
 * double numbers. The sequence can have a special "current element," which is
 * specified and accessed through four methods that are not available in the
 * sequence class (start, getCurrent, advance and isCurrent).
 *
 * <dl>
 * <dt><b>Limitations:</b> Beyond Int.MAX_VALUE elements, the size method does
 * not work.
 *
 * <dt><b>Note:</b>
 * <dd>This file contains only blank implementations ("stubs") because this is a
 * Programming Project for my students.
 *
 * <dt><b>Outline of Java Source Code for this class:</b>
 * <dd><A HREF="../../../../edu/colorado/collections/DoubleLinkedSeq.java"> http
 * ://www.cs.colorado.edu/~main/edu/colorado/collections/DoubleLinkedSeq.java
 * </A>
 * </dl>
 *
 * @author Fig
 * @author Wyatt J. Brown
 * @version Jan 24, 1999 */
public class DoubleLinkedSeq implements Cloneable
{

	/** Create a new sequence that contains all the elements from one sequence
	 * followed by another.
	 *
	 * @param s1 the first of two sequences.
	 * @param s2 the second of two sequences.
	 * @precondition Neither s1 nor s2 is null.
	 * @return a new sequence that has the elements of s1 followed by the
	 *         elements of s2 (with no current element).
	 * @exception NullPointerException Indicates that one of the arguments is
	 *            null.
	 * @exception OutOfMemoryError Indicates insufficient memory for the new
	 *            sequence. **/
	public static DoubleLinkedSeq catenation(final DoubleLinkedSeq s1,
			final DoubleLinkedSeq s2)
	{
		s1.tail.setLink(s2.head.getLink());
		final DoubleLinkedSeq sC = new DoubleLinkedSeq();
		sC.head = s1.head;
		sC.tail = s2.tail;
		return sC;
	}

	/** Returns a deep copy of a list of Node objects.
	 *
	 * @param source a Node object representing the head of a list.
	 * @return a Node object representing the head of the copy. */
	public static Node listCopy(Node source)
	{
		// Handle the special case of an empty list.
		if (source == null)
			return null;

		// Make the first node for the newly created list.
		final Node copyHead = new Node(source.getData(), null);
		Node copyTail = copyHead;

		// Copy the rest of the source list one node at a time.
		for (source = source.getLink(); source != null; source =
				source.getLink())
		{
			final Node aNode = new Node(source.getData(), null);
			copyTail.setLink(aNode);
			copyTail = aNode;
		}
		return copyHead;
	}

	private Node head, cursor, tail;

	private int manyNodes = 0;

	/** Initialize an empty sequence.
	 *
	 * @postcondition This sequence is empty. **/
	public DoubleLinkedSeq()
	{
		cursor = new Node(0.0, null);
		head = cursor;
		tail = cursor;
	}

	/** Add a new element to this sequence, after the current element.
	 *
	 * @param element the new element that is being added.
	 * @postcondition A new copy of the element has been added to this sequence.
	 *                If there was a current element, then the new element is
	 *                placed after the current element. If there was no current
	 *                element, then the new element is placed at the end of the
	 *                sequence. In all cases, the new element becomes the new
	 *                current element of this sequence.
	 * @exception OutOfMemoryError Indicates insufficient memory for a new
	 *            node. **/
	public void addAfter(final int element)
	{
		final Node node = new Node(element, cursor.getLink());

		cursor.setLink(node);
		cursor = node;

		if (node.getLink() == null)
			tail = node;

		++manyNodes;
	}

	/** Place the contents of another sequence at the end of this sequence.
	 *
	 * @param addend a sequence whose contents will be placed at the end of this
	 *        sequence.
	 * @precondition The parameter, addend, is not null.
	 * @postcondition The elements from addend have been placed at the end of
	 *                this sequence. The current element of this sequence
	 *                remains where it was, and the addend is also unchanged.
	 * @exception NullPointerException Indicates that addend is null.
	 * @exception OutOfMemoryError Indicates insufficient memory to increase the
	 *            size of this sequence. **/
	public void addAll(final DoubleLinkedSeq addend)
	{
		tail.setLink(addend.head.getLink());
		tail = addend.tail;
		manyNodes += addend.manyNodes;
	}

	/** Add a new element to this sequence, before the current element.
	 *
	 * @param element the new element that is being added.
	 * @postcondition A new copy of the element has been added to this sequence.
	 *                If there was a current element, then the new element is
	 *                placed before the current element. If there was no current
	 *                element, then the new element is placed at the start of
	 *                the sequence. In all cases, the new element becomes the
	 *                new current element of this sequence.
	 * @exception OutOfMemoryError Indicates insufficient memory for a new
	 *            node. **/
	public void addBefore(final int element)
	{
		cursor.setLink(new Node(cursor.getData(), cursor.getLink()));
		cursor.setData(element);
		++manyNodes;
	}

	/** Add a new element at the end of the sequence and make that element the
	 * current element.
	 *
	 * @param num a double to add at the end of the sequence. */
	public void addEnd(final double num)
	{
		final Node node = new Node(num, cursor.getLink());

		cursor.setLink(node);
		cursor = node;

		if (node.getLink() == null)
			tail = node;

		++manyNodes;
	}

	/** Add a new element at the front of the sequence and make it the current
	 * element.
	 *
	 * @param num a double to add at the front of the sequence. */
	public void addFront(final double num)
	{
		final Node node = new Node(num, null);

		if (head == null)
			tail = node;
		else
			node.setLink(head.getLink());

		head = node;
		cursor = node;

		++manyNodes;
	}

	/** Move forward, so that the current element is now the next element in
	 * this sequence.
	 *
	 * @precondition isCurrent() returns true.
	 * @postcondition If the current element was already the end element of this
	 *                sequence (with nothing after it), then there is no longer
	 *                any current element. Otherwise, the new element is the
	 *                element immediately after the original current element.
	 *
	 * @exception IllegalStateException Indicates that there is no current
	 *            element, so advance may not be called. **/
	public void advance()
	{
		cursor = cursor.getLink();
	}

	/** Generate a copy of this sequence.
	 *
	 * @return The return value is a copy of this sequence. Subsequent changes
	 *         to the copy will not affect the original, nor vice versa. Note
	 *         that the return value must be type cast to a DoubleLinkedSeq
	 *         before it can be used.
	 * @exception OutOfMemoryError Indicates insufficient memory for creating
	 *            the clone. **/
	@Override
	public Object clone()
	{ // Clone a DoubleLinkedSeq object.
		DoubleLinkedSeq answer;

		try
		{
			answer = (DoubleLinkedSeq) super.clone();
		} catch (final CloneNotSupportedException e)
		{ // This exception should not occur. But if it does, it would probably
			// indicate a programming error that made super.clone unavailable.
			// The most common error would be forgetting the "Implements
			// Cloneable"
			// clause at the start of this class.
			throw new RuntimeException(
					"This class does not implement Cloneable");
		}

		answer.head = listCopy(head);

		return answer;
	}

	/** Make the last element of the sequence the current element.
	 *
	 * @throws IllegalStateException if the sequence is empty. */
	public void currentLast()
	{
		if (manyNodes == 0)
			throw new IllegalStateException("The sequence is empty.");
		else
			cursor = tail;
	}

	/** Accessor method to get the current element of this sequence.
	 *
	 * @precondition isCurrent() returns true.
	 * @return the current capacity of this sequence
	 * @exception IllegalStateException Indicates that there is no current
	 *            element, so getCurrent may not be called. **/
	public double getCurrent()
	{
		return cursor.getData();
	}

	/** Accessor method to determine whether this sequence has a specified
	 * current element that can be retrieved with the getCurrent method.
	 *
	 * @return true (there is a current element) or false (there is no current
	 *         element at the moment) **/
	public boolean isCurrent()
	{
		return cursor != null;
	}

	/** Remove the current element from this sequence.
	 *
	 * @precondition isCurrent() returns true.
	 * @postcondition The current element has been removed from this sequence,
	 *                and the following element (if there is one) is now the new
	 *                current element. If there was no following element, then
	 *                there is now no current element.
	 * @exception IllegalStateException Indicates that there is no current
	 *            element, so removeCurrent may not be called. **/
	public void removeCurrent()
	{
		if (cursor.getLink() == null)
		{
			for (cursor = head; cursor.getLink().getLink() != null; cursor =
					cursor.getLink())
				;
			cursor.setLink(null);
		} else
		{
			final Node next = cursor.getLink();
			cursor.setLink(next.getLink());
			cursor.setData(next.getData());
			cursor = next;
		}
		--manyNodes;

	}

	/** Remove the element at the front of the sequence. If there is a next
	 * element, make that element the current element.
	 *
	 * @throws IllegalStateException if the sequence is empty. */
	public void removeFront()
	{
		head = head.getLink();
		cursor = head.getLink();

		--manyNodes;

		if (head == null)
			tail = null;
	}

	/** Return the ith element of the sequence and make current element the ith
	 * element.
	 *
	 * @param i an int that is the index of the element to return and make
	 *        current.
	 * @throws IllegalStateException if the sequence is empty.
	 * @return a double representing the ith element of the sequence. */
	public double retrieveElement(final int i)
	{
		setCurrent(i);
		return cursor.getData();
	}

	/** Make the ith element become the current element.
	 *
	 * @param i an int that is the index of the new current element.
	 * @throws IllegalStateException if the sequence is empty. */
	public void setCurrent(final int i)
	{
		if (manyNodes == 0)
			throw new IllegalStateException("The sequence is empty.");
		else
		{
			start();
			for (int iteration = 0; iteration <= i; ++iteration)
				advance();
		}
	}

	/** Determine the number of elements in this sequence.
	 *
	 * @return the number of elements in this sequence. **/
	public int size()
	{
		return manyNodes;
	}

	/** Set the current element at the front of this sequence.
	 *
	 * @postcondition The front element of this sequence is now the current
	 *                element (but if this sequence has no elements at all, then
	 *                there is no current element). **/
	public void start()
	{
		cursor = head;
	}
}
